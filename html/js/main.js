/**
 * Connects to the ROS Websocket Server and initialises the UI on connection.
 */
function connect() {

  /**
   * The default thresholds used by the blob detector
   */
  const thresholds = {
    h: [0, 255],
    s: [0, 255],
    v: [0, 255],
    area: [500, 20000]
  }

  /**
   * Create a ROS connection using roslibjs
   */
  var ros = new ROSLIB.Ros({
    url : `ws://${window.location.hostname}:9090`
  });

  /**
   * The error handler called if the ROS connection experiences an error
   */
  ros.on('error', function(error) {
    console.log('Error connecting to websocket server: ', error);
  });

  /**
   * The close handler called if the ROS connection closes.
   * This will hide the module UI and show the spinner while attempting to reconnect
   */
  ros.on('close', function() {
    console.log('Connection to websocket server closed.');
    $('#module-body').hide()
    $('#load-spinner').show()

    setTimeout(connect, 1000);
  });

  /**
   * The connection handler which is called when connected to the web bridge.
   * This callback will also initialise all of the ROS listeners and connect them to the UI.
   */
  ros.on('connection', function() {
    console.log('Connected to websocket server.');

    // We have a 2 second wait here as the web bridge typically  connects before the 
    // rest of ROS is ready and we don't want to show broken images
    setTimeout(function () {

      /**
       * Create a subscriber object for the masked image topic published by the example_demo script using roslibjs
       * @see http://robotwebtools.org/jsdoc/roslibjs/current/Topic.html
       */
      var cameraListener = new ROSLIB.Topic({
        ros : ros,
        name : '/image/masked_image/compressed',
        messageType : 'sensor_msgs/CompressedImage'
      });

      /**
       * Attach a callback to our image subscriber. Compressed images are in base64 format which can be 
       * displayed by setting the src attribute of a HTMLImageElement to the message data
       * @see http://robotwebtools.org/jsdoc/roslibjs/current/Topic.html#subscribe
       */
      cameraListener.subscribe(function(message) {
        $('#camera').attr('src', `data:image/png;base64,${message.data}`);
      });

      /**
       * Create our HSV Thresholds publisher object using roslibjs
       * @see http://robotwebtools.org/jsdoc/roslibjs/current/Topic.html
       */
      var topic = new ROSLIB.Topic({
        ros : ros,
        name : '/hsv_blob_following/thresholds',
        messageType : 'std_msgs/String'
      });

      /**
       * Hue Upper-Bound Slider input change callback.
       * This callback parses the value of the slider into an integer, updates the thresholds object
       * Then publishes the threshold object as a JSON string using our HSV threshold publisher
       * @see http://robotwebtools.org/jsdoc/roslibjs/current/Topic.html#subscribe
       */
      $('#h-high').on('input', function () {
        $('#h-high-txt').text(this.value);
        thresholds.h[1] = parseInt(this.value, 10);
        topic.publish(new ROSLIB.Message({data: JSON.stringify(thresholds)}));
      });

      /**
       * Hue Lower-Bound Slider input change callback.
       * This callback parses the value of the slider into an integer, updates the thresholds object
       * Then publishes the threshold object as a JSON string using our HSV threshold publisher
       * @see http://robotwebtools.org/jsdoc/roslibjs/current/Topic.html#subscribe
       */
      $('#h-low').on('input', function () {
        $('#h-low-txt').text(this.value);
        thresholds.h[0] = parseInt(this.value, 10);;
        topic.publish(new ROSLIB.Message({data: JSON.stringify(thresholds)}));
      });

      /**
       * Saturation Upper-Bound Slider input change callback.
       * This callback parses the value of the slider into an integer, updates the thresholds object
       * Then publishes the threshold object as a JSON string using our HSV threshold publisher
       * @see http://robotwebtools.org/jsdoc/roslibjs/current/Topic.html#subscribe
       */
      $('#s-high').on('input', function () {
        $('#s-high-txt').text(this.value);
        thresholds.s[1] = parseInt(this.value, 10);;
        topic.publish(new ROSLIB.Message({data: JSON.stringify(thresholds)}));
      });

      /**
       * Saturation Lower-Bound Slider input change callback.
       * This callback parses the value of the slider into an integer, updates the thresholds object
       * Then publishes the threshold object as a JSON string using our HSV threshold publisher
       * @see http://robotwebtools.org/jsdoc/roslibjs/current/Topic.html#subscribe
       */
      $('#s-low').on('input', function () {
        $('#s-low-txt').text(this.value);
        thresholds.s[0] = parseInt(this.value, 10);;
        topic.publish(new ROSLIB.Message({data: JSON.stringify(thresholds)}));
      });

      /**
       * Value Upper-Bound Slider input change callback.
       * This callback parses the value of the slider into an integer, updates the thresholds object
       * Then publishes the threshold object as a JSON string using our HSV threshold publisher
       * @see http://robotwebtools.org/jsdoc/roslibjs/current/Topic.html#subscribe
       */
      $('#v-high').on('input', function () {
        $('#v-high-txt').text(this.value);
        thresholds.v[1] = parseInt(this.value, 10);;
        topic.publish(new ROSLIB.Message({data: JSON.stringify(thresholds)}));
      });

      /**
       * Value Upper-Bound Slider input change callback.
       * This callback parses the value of the slider into an integer, updates the thresholds object
       * Then publishes the threshold object as a JSON string using our HSV threshold publisher
       * @see http://robotwebtools.org/jsdoc/roslibjs/current/Topic.html#subscribe
       */
      $('#v-low').on('input', function () {
        $('#v-low-txt').text(this.value);
        thresholds.v[0] = parseInt(this.value, 10);;
        topic.publish(new ROSLIB.Message({data: JSON.stringify(thresholds)}));
      });

      /**
       * Area Higher-Bound Slider input change callback.
       * This callback parses the value of the slider into an integer, updates the thresholds object
       * Then publishes the threshold object as a JSON string using our HSV threshold publisher
       * @see http://robotwebtools.org/jsdoc/roslibjs/current/Topic.html#subscribe
       */
      $('#area-high').on('input', function () {
        $('#area-high-txt').text(this.value);
        thresholds.area[1] = parseInt(this.value, 10);;
        topic.publish(new ROSLIB.Message({data: JSON.stringify(thresholds)}));
      });

      /**
       * Area Lower-Bound Slider input change callback.
       * This callback parses the value of the slider into an integer, updates the thresholds object
       * Then publishes the threshold object as a JSON string using our HSV threshold publisher
       * @see http://robotwebtools.org/jsdoc/roslibjs/current/Topic.html#subscribe
       */
      $('#area-low').on('input', function () {
        $('#area-low-txt').text(this.value);
        thresholds.area[0] = parseInt(this.value, 10);;
        topic.publish(new ROSLIB.Message({data: JSON.stringify(thresholds)}));
      });

      // Hides the spinner and shows the module body
      $('#load-spinner').hide()
      $('#module-body').show()
    }, 2000);
  });
}

// Connect when the DOM has finalised
$(document).ready(connect);
