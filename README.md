# Robotic Vision Web User Interface (RV Web UI)

## Overview

This is an example demonstrator package for the [Robotic Vision Web User Interface (RV Web UI)](https://bitbucket.org/acrv/rv_webui). The example demonstrates creating a simple HSV blob detector that integrates with ROS.

*NOTE: While we discuss using demonstrators with ROS within this example, demonstrators can be built for any arbitrary set of software*

## Setup

### Requirements

- Ubuntu 18.04
- ROS Melodic
- RV Web UI

## Creating Demonstrators

Each demonstrator is comprised of a module file (`module.yml`) which describes the demonstrator and its behaviour, and a set of HTML files that provide the user interface for the demonstrator.

Additionally, demonstrators will typically contain a number of scripts, such as python scripts or ROS launch scripts that are required by the demonstrator to operate.

### Module File

Demonstrator packages are located by RV Web UI through the existance of a `module.yml` file located in the root directory of the package. This file provides a description of the module, as well as a set of user instructions, and launch instructions used by RV Web UI to launch the demonstrator.

The `module.yml` is comprised of the following fields:

| Parameter        | Type | Description  |
| ------------- |----|-----------|
| name      | String | The name of the module as it will appear in RV Web UI. |
| description  | String | A short description of the module which will be displayed in the RV Web UI.  |
| instructions  | HTML | Instructions that will be displayed to the user just prior to running the demo. These should describe how to set up as well as use the demonstrator.  |
| run  | Array[String] | A list of bash commands that need to be executed in order to set up the demonstrator.  |

### HTML Files

The interface for each demonstrator is defined as a set of HTML and JS/CSS files, located in the `html` directory found in the root directory of the package. Within this set of files, the only required file is a `html/index.html` file which will be loaded automatically into the browser when the user starts the demonstrator.

For simplicity, RV Web UI provides a number of standard dependencies for creating interactive interfaces, such as JQuery, ROSLibJS, Bootstrap.

We provide `html/index.html` as an example of how to set up an interactive interface.
