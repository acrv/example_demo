#!/usr/bin/env python

# IMPORT CLASSES
import cv2
import math
import json
import rospy
import numpy as np
from cv_bridge import CvBridge

# IMPORT MESSAGE TYPES
from sensor_msgs.msg import Image, CompressedImage
from std_msgs.msg import String


### CLASS TO HANDLE ALL ROS STUFF ###
class ExampleDemo():
	
	# INITIALISATION
	def __init__(self):
		# HSV and Contour Thresholds
		self.hueThresholds = [0, 180]
		self.satThresholds = [0, 255]
		self.valueThresholds = [0, 255]
		self.areaThresholds = [500, 20000]

		# converts from ROS image to OpenCV image
		self.bridge = CvBridge()
		
		# ROS PUBLISHERS 
		# publish masked image, this can be displayed using RV_WebUI
		self.maskedImagePublisher = rospy.Publisher('/image/masked/compressed', CompressedImage, queue_size=1)

		# ROS SUBSCRIBERS
		self.image_subscriber = rospy.Subscriber('/image', Image, self.Image_Callback)
		self.blobThresholdsSubscriber = rospy.Subscriber('/thresholds', String, self.image_callback)

	def run(self):
		# Main ROS Loop in Try-Except Case
		try:
			rospy.spin()
		except KeyboardInterrupt:
			pass
		except Exception as e:
			rospy.logerr(str(e))

	def image_callback(self, data):

		try:
			# Attempt to convert ros image to cv2 8-bit bgr image
			img_bgr = self.bridge.imgmsg_to_cv2(data, "bgr8")

			# Resize image
			img_bgr = cv2.resize(img_bgr, (480, 320))

			# Detect Blob Center
			blobCenter, contourArea, img_masked = self._detect_blob(img_bgr)
			
			# Publish masked image
			self.maskedImagePublisher.publish(self.bridge.cv2_to_compressed_imgmsg(img_masked, "png"))

		except Exception as e:
			rospy.logwarn("In Blob Follow Demo Node Image_Callback Function. Error: " + str(e))


	def _detect_blob(self, img_bgr):
		# setup variables
		centerPoint = None
		largestContour = None
		largestContourArea = 0
		blobFound = False

		# Pre-process image using gaussian blur with 5x5 kernel and 0 standard deviation
		img_bgr = cv2.GaussianBlur(img_bgr, (5,5), 0)

		# Threshold the image using the HSV values
		img_bw, img_masked = self._threshold_image(img_bgr)

		# Many ways to do blob filtering in cv2. Going to use contour method
		_, contours, _ = cv2.findContours(img_bw, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

		# Filter areas and take largest contour greater than the minimum area
		for con in contours:
			area = cv2.contourArea(con)
			if (area > self.areaThresholds[0] and area < self.areaThresholds[1]) and (blobFound == False or area > cv2.contourArea(largestContour)):
				largestContour = con
				largestContourArea = area
				blobFound = True

		# Get centre point of contour
		if blobFound == True:
			m = cv2.moments(largestContour)
			cx = int(m["m10"] / m["m00"])
			cy = int(m["m01"] / m["m00"])

			centerPoint = [cx, cy]

		# Draw contour on the masked image in green
		if blobFound == True:
			cv2.drawContours(img_masked, [largestContour], 0, (0, 255, 0), 3)

		return centerPoint, largestContourArea, img_masked


	def _threshold_image(self, img_bgr):
		# Convert bgr image to hsv image
		img_hsv = cv2.cvtColor(img_bgr, cv2.COLOR_BGR2HSV)

		# Due to wrap around of hue value in the HSV colour space need to check if min value is greater than max value
		if self.hueThresholds[0] > self.hueThresholds[1]:
			# Will need to do threshold from min to 180 and from 0 to maximum to get correct hue thresholded image

			# First do min to 180 hue threshold. Create numpy arrays for min and max hsv thresholds
			lower_thresh = np.array([self.hueThresholds[0], self.satThresholds[0], self.valueThresholds[0]])
			upper_thresh = np.array([180, self.satThresholds[1], self.valueThresholds[1]])

			# Threshold image
			img_bw1 = cv2.inRange(img_hsv, lower_thresh, upper_thresh)

			# First do min to 180 hue threshold. Create numpy arrays for min and max hsv thresholds
			lower_thresh = np.array([0, self.satThresholds[0], self.valueThresholds[0]])
			upper_thresh = np.array([self.hueThresholds[1], self.satThresholds[1], self.valueThresholds[1]])

			# Threshold image
			img_bw2 = cv2.inRange(img_hsv, lower_thresh, upper_thresh)

			# Add threshold images together using bitwise or
			img_bw = cv2.bitwise_or(img_bw1, img_bw2)

		else:
			# Simply need to threshold from hue min to hue max
			
			# Create numpy arrays for min and max hsv thresholds
			lower_thresh = np.array([self.hueThresholds[0], self.satThresholds[0], self.valueThresholds[0]])
			upper_thresh = np.array([self.hueThresholds[1], self.satThresholds[1], self.valueThresholds[1]])

			# Threshold image
			img_bw = cv2.inRange(img_hsv, lower_thresh, upper_thresh)

		# Apply img_bw (which is binary mask to bgr image). This masked image can be used to for viewing.
		img_masked = cv2.bitwise_and(img_bgr, img_bgr, mask=img_bw)

		# Return img_bw and img_masked
		return img_bw, img_masked


	def BlobThresholds_Callback(self, msg):
                data = json.loads(msg.data)
                self.hueThresholds = data['h']
                self.satThresholds = data['s']
                self.valueThresholds = data['v']
                self.areaThresholds = data['area']
		
if __name__ == '__main__':

	# ROS NODE INIT
	rospy.init_node('example_demo')
	
	# Create and run an instance of the Example class
	demo = ExampleDemo()
	demo.run()



